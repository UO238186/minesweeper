# EII'S MINESWEEPER #

## What is this repository for? ##

A minesweeper development project, just for fun.

Created by students from the School of Computer Science, University of Oviedo.

Version: 0.0.0


## How do I get set up? ##

This is an Eclipse project. Importing with Eclipse should be enough.

## Contribution guidelines ##

* You are welcome to contribute, specially if you want to practice developing an app with Window Builder.

* Try to implement your code in the easiest to understand way possibe:

	* Write concise and explanatory comments.

	* Follow the general design of the application in its current state.

## What can I work on at the moment? ##

The core mechanics are almost finished. There is still a lot of work in the GUI and some secondary aspects of gameplay.

### Some ideas of what to do: ###

* Implement a way for the player to mark suspicious cells without danger (and track remaining unmarked bombs).

* Extend the existing GUI with Window Builder.

* Create graphical assets the application could use.

* Think of cool stuff to do and add it here!

## Who do I talk to? ##

People willing to answer questions regarding this project / repo:

* Íñigo Gutiérrez Fernández (inigogf.95@gmail.com)

## Other stuff ##

[Markdown (readme formatting) tutorial](https://bitbucket.org/tutorials/markdowndemo)