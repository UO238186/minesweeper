package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logic.Cell;
import logic.Game;

import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class MainWindow extends JFrame {
	
	public final static int LEFT_CLICK = MouseEvent.BUTTON1;
	public final static int RIGHT_CLICK = MouseEvent.BUTTON3;
	
	public final static String FLAG_ICON = "/img/flag.png";
	
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnGame;
	private JMenu mnOptions;
	private JMenu mnHelp;
	private JMenuItem mntmNewGame;
	private JSeparator separator;
	private JMenuItem mntmExit;
	private JMenuItem mntmConfigGame;
	private JMenuItem mntmAbout;
	private JSeparator separator_1;
	private JMenuItem mntmHowToPlay;
	private JPanel gridPanel;
	private JPanel displayPanel;
	private JTextField txtFlags;
	private JButton btnNewGame;
	private JTextField txtTime;
	
	private Game game = new Game();
	private CellEventHandler buttonEvent = new CellEventHandler();
	private JButton b[][] = new JButton[game.getHeight()][game.getWidth()];
	private JLabel lblFlags;
	private JLabel lblTime;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Process the logic when the player left-clicks in a cell
	 * @param x pos
	 * @param y pos
	 */
	private void leftClick(int x, int y) {
		String number = game.getGrid()[x][y].getNumber();
		b[x][y].setText(number);
		game.getGrid()[x][y].setRevealed(true);
		// If bomb, reveal all cells and end game.
		if(number.equals(Character.toString(Game.BOMB_CHAR))){
			revealGrid();
			JOptionPane.showMessageDialog(this, "You stepped on a bomb. GAME OVER.");
		}
		// If empty, reveal all connected empty cells and adjacent numbers.
		else if (number.equals(" ")){
			game.checkDirections(x,y);
			//updateGrid();
		}
		else{
			b[x][y].setEnabled(false);
		}
	} 
	
	/**
	 * 
	 * @param x
	 * @param y
	 */
	private void rightClick(int x, int y) {
		if(b[x][y].isEnabled())
			game.toggleFlag(x, y);
		if (game.playerWon()) {
			revealGrid();
			JOptionPane.showMessageDialog(this, "You won the game!!!");
		}
	}
	
	/**
	 * Reveals the grid 
	 */
	private void revealGrid() {
		for(int i=0 ; i < game.getHeight(); i++){
			for(int j=0 ; j < game.getWidth(); j++){
				game.getCell(i, j).setRevealed(true);
			}
		}
		updateGrid();
	}
	
	/**
	 * Resets the game
	 */
	protected void resetGame() {
		b = new JButton[game.getHeight()][game.getWidth()];
		createGridButtons();
		game.removeGrid();
	}
	
	/**
	 * Returns the game instance
	 * @return
	 */
	protected Game getGame() {
		return game;
	}
	
	/**
	 * Creates the configuration dialog
	 */
	private void createConfigurationDialog() {
		GameConfigurationDialog dialog = new GameConfigurationDialog(this);
		dialog.setModal(true);
		dialog.setLocationRelativeTo(this);
		dialog.setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/img/minefieldInverted64Painted.png")));
		setTitle("Minesweeper");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 639, 477);
		setJMenuBar(getMenuBar_1());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getGridPanel(), BorderLayout.CENTER);
		contentPane.add(getDisplayPanel(), BorderLayout.NORTH);
	}

	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnGame());
			menuBar.add(getMnOptions());
			menuBar.add(getMnHelp());
		}
		return menuBar;
	}
	private JMenu getMnGame() {
		if (mnGame == null) {
			mnGame = new JMenu("Game");
			mnGame.setMnemonic('g');
			mnGame.add(getMntmNewGame());
			mnGame.add(getSeparator());
			mnGame.add(getMntmExit());
		}
		return mnGame;
	}
	private JMenu getMnOptions() {
		if (mnOptions == null) {
			mnOptions = new JMenu("Options");
			mnOptions.setMnemonic('p');
			mnOptions.add(getMntmConfigGame());
		}
		return mnOptions;
	}
	private JMenu getMnHelp() {
		if (mnHelp == null) {
			mnHelp = new JMenu("Help");
			mnHelp.setMnemonic('h');
			mnHelp.add(getMntmHowToPlay());
			mnHelp.add(getSeparator_1());
			mnHelp.add(getMntmAbout());
		}
		return mnHelp;
	}
	private JMenuItem getMntmNewGame() {
		if (mntmNewGame == null) {
			mntmNewGame = new JMenuItem("New Game");
			mntmNewGame.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					resetGame();
				}
			});
			mntmNewGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
			mntmNewGame.setMnemonic('n');
		}
		return mntmNewGame;
	}
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
	private JMenuItem getMntmExit() {
		if (mntmExit == null) {
			mntmExit = new JMenuItem("Exit");
			mntmExit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
		}
		return mntmExit;
	}
	private JMenuItem getMntmConfigGame() {
		if (mntmConfigGame == null) {
			mntmConfigGame = new JMenuItem("Config. Game");
			mntmConfigGame.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					createConfigurationDialog();
				}
			});
			mntmConfigGame.setMnemonic('c');
		}
		return mntmConfigGame;
	}
	private JMenuItem getMntmAbout() {
		if (mntmAbout == null) {
			mntmAbout = new JMenuItem("About");
			mntmAbout.setMnemonic('b');
		}
		return mntmAbout;
	}
	private JSeparator getSeparator_1() {
		if (separator_1 == null) {
			separator_1 = new JSeparator();
		}
		return separator_1;
	}
	private JMenuItem getMntmHowToPlay() {
		if (mntmHowToPlay == null) {
			mntmHowToPlay = new JMenuItem("How to play");
			mntmHowToPlay.setMnemonic('h');
		}
		return mntmHowToPlay;
	}
	
	/**
	 * Removes all buttons from grid panel, then creates as many as needed and adds them to this panel.
	 */
	private void createGridButtons() {
		gridPanel.removeAll();
		gridPanel.revalidate();
		gridPanel.setLayout(new GridLayout(game.getHeight(), game.getWidth()));
		for (int i = 0; i < game.getHeight(); i++) {
			for (int j = 0; j < game.getWidth(); j++) {
				b[i][j] = new JButton();
				b[i][j].setActionCommand(String.format("%d,%d", i, j));
			    b[i][j].addMouseListener(buttonEvent);
			    b[i][j].setText("");
			    b[i][j].setEnabled(true);
			    gridPanel.add(b[i][j]);
			}
		}
	}
	
	private JPanel getGridPanel() {
		if (gridPanel == null) {
			gridPanel = new JPanel();
			createGridButtons();
		}
		return gridPanel;
	}
	private JPanel getDisplayPanel() {
		if (displayPanel == null) {
			displayPanel = new JPanel();
			displayPanel.setLayout(new GridLayout(0, 5, 0, 0));
			displayPanel.add(getLblFlags());
			displayPanel.add(getTxtFlags());
			displayPanel.add(getBtnNewGame());
			displayPanel.add(getLblTime());
			displayPanel.add(getTxtTime());
		}
		return displayPanel;
	}
	private JTextField getTxtFlags() {
		if (txtFlags == null) {
			txtFlags = new JTextField();
			txtFlags.setEditable(false);
			txtFlags.setColumns(10);
		}
		return txtFlags;
	}
	private JButton getBtnNewGame() {
		if (btnNewGame == null) {
			btnNewGame = new JButton("New Game");
			btnNewGame.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					resetGame();
				}
			});
		}
		return btnNewGame;
	}
	private JTextField getTxtTime() {
		if (txtTime == null) {
			txtTime = new JTextField();
			txtTime.setEditable(false);
			txtTime.setColumns(10);
		}
		return txtTime;
	}
	
	private class CellEventHandler extends MouseAdapter {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			
			//Get position via action command
			String[] coor = ((AbstractButton) e.getSource()).getActionCommand().split(",");
			int x = Integer.parseInt(coor[0]);
			int y = Integer.parseInt(coor[1]);
			
			if ( game.getGrid() == null ) {
				game.generateGrid(x, y);
			}
			
			//Depending of the button of the mouse do different actions
			if (e.getButton() == LEFT_CLICK) {
				leftClick(x, y);
			} else if (e.getButton() == RIGHT_CLICK) {
				rightClick(x, y);
			}
			
			updateStateOfTheGame();
		}
	}
	
	private void updateStateOfTheGame() {
		updateGrid();
		updateTextFields();
	}
	
	private void updateTextFields() {
		this.getTxtFlags().setText(String.format("%d", game.getFlaggedCells()));
	}
	
	/**
	 * Updates text and state of revealed cells.
	 */
	public void updateGrid() {
		for(int i=0 ; i < game.getHeight(); i++){
			for(int j=0 ; j < game.getWidth(); j++){
				Cell cell = game.getCell(i, j);
				if (cell.isFlagged() && !cell.isRevealed()) {
					setCellIcon(b[i][j] ,FLAG_ICON);
				} else if(game.getCell(i,j).isRevealed()){
					b[i][j].setText(cell.getNumber());
					b[i][j].setEnabled(false);
				} else {
					b[i][j].setIcon(null);
				}
			}
		 }
	}
	
	private void setCellIcon(JButton cell, String URL) {
		cell.setIcon(new ImageIcon(MainWindow.class.getResource(URL)));
	}
	
	private JLabel getLblFlags() {
		if (lblFlags == null) {
			lblFlags = new JLabel("Flags:");
			lblFlags.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblFlags;
	}
	private JLabel getLblTime() {
		if (lblTime == null) {
			lblTime = new JLabel("Time:");
			lblTime.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblTime;
	}
}
