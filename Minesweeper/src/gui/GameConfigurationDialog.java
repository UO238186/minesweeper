package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logic.Game;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class GameConfigurationDialog extends JDialog {

	private MainWindow mainWindow;
	
	private final JPanel contentPanel = new JPanel();
	private JLabel lblGridHeight;
	private JLabel lblGridWidth;
	private JLabel lblBombs;
	private JSpinner spGridHeight;
	private JSpinner spGridWidth;
	private JSpinner spBombs;
	
	/**
	 * Saves the changes made by the user
	 */
	private void saveChanges() {
		Game game = mainWindow.getGame();
		game.setHeight((int) getSpGridHeight().getValue());
		game.setWidth((int) getSpGridWidth().getValue());
		game.setBombs((int) getSpBombs().getValue());
		mainWindow.resetGame();
	}

	/**
	 * Create the dialog.
	 */
	public GameConfigurationDialog(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
		setTitle("Configuration of the game");
		setResizable(false);
		setBounds(100, 100, 301, 170);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		contentPanel.add(getLblGridHeight());
		contentPanel.add(getLblGridWidth());
		contentPanel.add(getLblBombs());
		contentPanel.add(getSpGridHeight());
		contentPanel.add(getSpGridWidth());
		contentPanel.add(getSpBombs());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setMnemonic('o');
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						saveChanges();
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setMnemonic('c');
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	private JLabel getLblGridHeight() {
		if (lblGridHeight == null) {
			lblGridHeight = new JLabel("Grid height:");
			lblGridHeight.setDisplayedMnemonic('h');
			lblGridHeight.setLabelFor(getSpGridHeight());
			lblGridHeight.setBounds(10, 11, 80, 14);
		}
		return lblGridHeight;
	}
	private JLabel getLblGridWidth() {
		if (lblGridWidth == null) {
			lblGridWidth = new JLabel("Grid width:");
			lblGridWidth.setDisplayedMnemonic('w');
			lblGridWidth.setLabelFor(getSpGridWidth());
			lblGridWidth.setBounds(10, 36, 80, 14);
		}
		return lblGridWidth;
	}
	private JLabel getLblBombs() {
		if (lblBombs == null) {
			lblBombs = new JLabel("Bombs:");
			lblBombs.setDisplayedMnemonic('b');
			lblBombs.setLabelFor(getSpBombs());
			lblBombs.setBounds(10, 61, 80, 14);
		}
		return lblBombs;
	}
	private JSpinner getSpGridHeight() {
		if (spGridHeight == null) {
			spGridHeight = new JSpinner();
			spGridHeight.setModel(new SpinnerNumberModel(new Integer(mainWindow.getGame().getHeight()), new Integer(1), new Integer(Game.MAX_HEIGHT), new Integer(1)));
			spGridHeight.setBounds(100, 8, 62, 20);
		}
		return spGridHeight;
	}
	private JSpinner getSpGridWidth() {
		if (spGridWidth == null) {
			spGridWidth = new JSpinner();
			spGridWidth.setModel(new SpinnerNumberModel(new Integer(mainWindow.getGame().getWidth()), new Integer(1), new Integer(Game.MAX_WIDTH), new Integer(1)));
			spGridWidth.setBounds(100, 33, 62, 20);
		}
		return spGridWidth;
	}
	private JSpinner getSpBombs() {
		if (spBombs == null) {
			spBombs = new JSpinner();
			spBombs.setModel(new SpinnerNumberModel(new Integer(mainWindow.getGame().getBombs()), new Integer(1), new Integer(Game.MAX_BOMBS), new Integer(1)));
			spBombs.setBounds(100, 58, 62, 20);
		}
		return spBombs;
	}
}
