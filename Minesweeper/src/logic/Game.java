package logic;

import java.util.Random;

public class Game {

	public static final int DEFAULT_BOMBS = 10;
	public static final int DEFAULT_WIDTH = 10;
	public static final int DEFAULT_HEIGHT = 10;
	
	public static final int MAX_BOMBS = 15;
	public static final int MAX_WIDTH = 30;
	public static final int MAX_HEIGHT = 30;

	public static final char BOMB_CHAR = 'B';
	public static final String PROTECTED = "P";
	public static final String EMPTY_CELL = " ";
	
	private Cell[][] grid; // Stores the cells
	private int bombs; // Number of bombs in the grid
	private int width; // Width of the grid in cells
	private int height; // Height of the  grid in cells
	
	private int flaggedCells; //Number of cells flagged
	
	/**
	 * Initializes variables to their default value and creates the grid of cells.
	 */
	public Game() {
		setBombs(DEFAULT_BOMBS);
		setWidth(DEFAULT_WIDTH);
		setHeight(DEFAULT_HEIGHT);
		grid = null; // Checked by first button clicked, to generate grid afterwards
	}

	/**
	 * Getter for bombs.
	 * @return Number of bombs.
	 */
	public int getBombs() {
		return bombs;
	}

	/**
	 * Setter for bombs.
	 * @param bombs - Bombs for the game to have.
	 */
	public void setBombs(int bombs) {
		this.bombs = bombs;
	}

	/**
	 * Getter for width of grid.
	 * @return Number of columns of the grid.
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Setter for width of grid.
	 * @param width - Number of columns to set the grid to.
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Getter for height of grid.
	 * @return Number of rows of the grid.
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Setter for height of the grid.
	 * @param height - Number of rows to set the grid to.
	 */
	public void setHeight(int height) {
		this.height = height;
	}
	
	/**
	 * Returns if the player won
	 * @return
	 */
	public boolean playerWon() {
		if (flaggedCells == bombs) {
			for ( int i = 0; i < height; i++ ) {
				for ( int j = 0; j < width; j++ ) {
					Cell cell = getCell(i, j);
					if (cell.hasBomb() ^ cell.isFlagged()) //Xor gate
						return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Changes if a cell is flagged or not. If not flagged, puts a flag on it and viceversa.
	 * Also, it updates the number of flagged cells
	 * @param x
	 * @param y
	 */
	public void toggleFlag(int x, int y) {
		Cell cell = getCell(x, y);
		if (cell.isFlagged()) {
			flaggedCells--;
			cell.setFlagged(false);
		} else {
			flaggedCells++;
			cell.setFlagged(true);
		}
	}
	
	/**
	 * Sets the bombs to random positions and updates the number of each cell.
	 */
	public void generateGrid() {
		grid = new Cell[height][width];
		
		for ( int i = 0; i < height; i++ ) {
			for ( int j = 0; j < width; j++ ) {
				grid[i][j] = new Cell();
			}
		}
		
		bombPlanter(width * height); // Now we place the bombs
		
		for ( int i = 0; i < height; i++ ) { // For each cell of the grid, update its number
			for ( int j = 0; j < width; j++ ) {
				updateNumber(i, j);
			}
		}
		System.out.println(toString());
	}
	
	/**
	 * Sets the bombs to random positions and updates the number of each cell.
	 * Receives as a parameter a position and assures a bomb is not planted there.
	 * @param row - Row of first click
	 * @param col - Col of first click
	 */
	public void generateGrid(int row, int col) {
		grid = new Cell[height][width];
		
		for ( int i = 0; i < height; i++ ) {
			for ( int j = 0; j < width; j++ ) {
				grid[i][j] = new Cell();
			}
		}
		
		int protectedCells = 9; // bombPlanter() must know this to calculate number of cells where bombs can be planted
		for ( int i = -1; i <= 1; i++ ) {
			for ( int j = -1; j <= 1; j++ ) {
				try {
					grid[row + i][col + j].setNumber(String.valueOf(PROTECTED));
				} catch (ArrayIndexOutOfBoundsException e) {
					protectedCells--;
				}
			}
		}
		
		bombPlanter( ( width * height ) - protectedCells); // Now we place the bombs
		
		for ( int i = 0; i < height; i++ ) { // For each cell of the grid, update its number
			for ( int j = 0; j < width; j++ ) {
				updateNumber(i, j);
			}
		}
		System.out.println(toString());
	}

	/**
	 * Updates number of cell at position (row, col) so it is the number of adjacent bombs.
	 * @param row - Row of the cell to update
	 * @param col - Column of the cell to update
	 */
	private void updateNumber(int row, int col) {
		int counter = 0;
		for ( int i = row - 1; i <= row + 1; i++ ) {
			for ( int j = col - 1; j <= col + 1; j++ ) {
				try {
					if ( grid[i][j].hasBomb() ) {
						counter++; // Cells with bombs will count themselves, but their number is not relevant
					}
					
				} catch (ArrayIndexOutOfBoundsException e) {
					// Avoid throwing an exception for cells that don't exist
				}
			}
		}
		if(counter != 0){
			if(grid[row][col].hasBomb()){
				grid[row][col].setNumber(Character.toString(BOMB_CHAR));
			}
			else{
				grid[row][col].setNumber(Integer.toString(counter));
			}
		}
		else{
			grid[row][col].setNumber(EMPTY_CELL);
		}
	}
	
	/**
	 * Returns the contents of the grid as a string.
	 * B is a bomb, other cells are shown as their number.
	 * @return Contents of the grid as a string.
	 */
	public String toString() {
		String output = "";
		for ( int row = 0; row < height; row++ ) {
			for ( int col = 0; col < width; col++ ) {
				output += " ";
				if ( grid[row][col].hasBomb() ) {
					output += BOMB_CHAR;
				}
				else {
					output += grid[row][col].getNumber();
				}
			}
			output += "\n";
		}
		return output;
	}
	
	/**
	 * Distributes the bombs in the grid proportionally to the remaining number of cells
	 * available.
	 * @param numberOfCellsRemaining 
	 */
	private void bombPlanter(int numberOfCellsRemaining){
		
		int numberOfBombs = bombs;
		
		Random generator = new Random();
		
		for(int i = 0; i < height; i++){
			for(int j = 0 ; j < width; j++){
				if ( grid[i][j].getNumber() != PROTECTED ) {
					if (generator.nextInt(numberOfCellsRemaining) < numberOfBombs ){
						grid[i][j].setBomb(true);
						grid[i][j].setNumber(Character.toString(BOMB_CHAR));
						numberOfBombs-- ;
					}
					numberOfCellsRemaining--;
				}
			}
		}
	}

	/**
	 * Returns a reference to the grid.
	 * @return A reference to the grid.
	 */
	public Cell[][] getGrid() {
		return grid;
	}
	
	/**
	 * Returns a reference to cell at position [row, col] of the grid.
	 * @return A reference to cell at position [row, col] of the grid.
	 */
	public Cell getCell( int row, int col ) {
		return grid[row][col];
	}
	
	/**
	 * If cell at [row, col] is empty, reveals all the empty cells
	 * connected to it by other empty cells and cells adjacent to them.
	 * @param row
	 * @param col
	 */
	public void checkDirections(int row,int col){
		if(grid[row][col].getNumber().equals(EMPTY_CELL)){
			grid[row][col].setRevealed(true);
			// check surrounding cells for more empty cells
			for ( int i = row - 1; i <= row+1; i++ ) {
				for ( int j = col - 1; j <= col+1; j++ ) {
					try {
						if ( !grid[i][j].isRevealed() ) {
							checkDirections(i,j);
						}
					}
					catch (ArrayIndexOutOfBoundsException e) {
						// do nothing with cells which do not exist and avoid throwing an exception
					}
				}
			}
			// Reveal adjacent cells (numbers, although it iterates through empty cells already revealed)
			for ( int i = row - 1; i <= row+1; i++ ) {
				for ( int j = col - 1; j <= col+1; j++ ) {
					try {
						grid[i][j].setRevealed(true);
					}
					catch (ArrayIndexOutOfBoundsException e) {
						// do nothing with cells which do not exist and avoid throwing an exception
					}
				}
			}
		}
	}

	public void removeGrid() {
		flaggedCells = 0;
		grid = null;
	}

	public int getFlaggedCells() {
		return flaggedCells;
	}
	
}
