package logic;


public class Cell {
	
	private boolean bomb; // whether the cell has a bomb
	private boolean flagged; // whether the cell has a flag
	private boolean revealed; // whether the contents of the cell are known to the player
	private String number; // number to be displayed if the cell does not contain a bomb and is adyacent to one

	public Cell() {
		bomb = false;
		revealed = false;
		number = "0";
	}
	
	/**
	 * Getter for bomb
	 * @return Whether the cell has a bomb
	 */
	public boolean hasBomb() {
		return bomb;
	}

	/**
	 * Setter for bomb
	 * @param bomb - True if the cell is to have a bomb, false otherwise.
	 */
	public void setBomb(boolean bomb) {
		this.bomb = bomb;
	}
	
	/**
	 * Getter for revealed
	 * @return Whether the contents of the cell are known to the player
	 */
	public boolean isRevealed() {
		return revealed;
	}
	
	/**
	 * Setter for revealed
	 * @param revealed - True if the cell is to be revealed, false otherwise.
	 */
	public void setRevealed(boolean revealed) {
		this.revealed = revealed;
	}
	
	/**
	 * Getter for number
	 * @return Number of adjacent bombs (if number is updated)
	 */
	public String getNumber() {
		return number;
	}
	
	/**
	 * Setter for number
	 * @param number - Should be the number of adjacent bombs
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	public boolean isFlagged() {
		return flagged;
	}

	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}
	
}
