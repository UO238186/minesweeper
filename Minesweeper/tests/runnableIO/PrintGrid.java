package runnableIO;

import logic.Game;

/**
 * Creates a new grid and prints it on the console, as it would be at app initialization
 */
public class PrintGrid {

	public static void main(String[] args) {
		System.out.println(new Game().toString());
	}

}
